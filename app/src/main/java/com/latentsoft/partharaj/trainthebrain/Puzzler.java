package com.latentsoft.partharaj.trainthebrain;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by parth on 10/28/2017.
 */

public class Puzzler {
    ArrayList<String> colorString = new ArrayList<>();
    ArrayList<String> colorCodes = new ArrayList<>();

    ArrayList<String> pickedColorCodes = new ArrayList<>();

    private String correctColorCode;

    private int pickedPosition;

    public Puzzler() {
        init();
    }

    public void generate(){
        pickedPosition = (int) (Math.floor(Math.abs(Math.random()*10)) % colorString.size());
        Log.e("PD_DEBUG", "Picked Position : "+pickedPosition);
    }

    public String getColorString(){
        return colorString.get(pickedPosition);
    }

    public ArrayList<String> getColorCodes(){
        pickedColorCodes.add(colorCodes.get(pickedPosition));
        correctColorCode = colorCodes.get(pickedPosition);

        ArrayList<String> tempColors = colorCodes;
        tempColors.remove(pickedPosition);
        Collections.shuffle(tempColors);

        for (int i= 0; i< 3; i++) {
            pickedColorCodes.add(tempColors.get(i));
        }

        Collections.shuffle(pickedColorCodes);

        return pickedColorCodes;
    }

    public String getCorrectColorCode(){
        return correctColorCode;
    }

    public int getPickedPosition() {
        return pickedPosition;
    }

    private void init() {
        colorString.add("Blue");
        colorString.add("Red");
        colorString.add("White");
        colorString.add("Green");
        colorString.add("Yellow");
        colorString.add("Black");
        colorString.add("Pink");
        colorString.add("Orange");
        colorString.add("Purple");

        colorCodes.add("#2196F3");
        colorCodes.add("#F44336");
        colorCodes.add("#ffffff");
        colorCodes.add("#4CAF50");
        colorCodes.add("#FFEB3B");
        colorCodes.add("#000000");
        colorCodes.add("#E91E63");
        colorCodes.add("#FF9800");
        colorCodes.add("#9C27B0");
    }
}
