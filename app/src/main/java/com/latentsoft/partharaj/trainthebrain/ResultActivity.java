package com.latentsoft.partharaj.trainthebrain;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    TextView tvCurrentScore, tvHighestScore;
    Button btnPlayAgain;

    SharedPreferences sp;

    int currentScore = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        tvCurrentScore = (TextView) findViewById(R.id.tvCurrentScore);
        tvHighestScore = (TextView) findViewById(R.id.tvHighestScore);
        btnPlayAgain = (Button) findViewById(R.id.btnPlayAgain);

        btnPlayAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ResultActivity.this, MainActivity.class));
                finish();
            }
        });

        Intent intent = getIntent();

        currentScore = intent.getIntExtra("score", -1);
        if (currentScore == -1) {
            tvCurrentScore.setVisibility(View.GONE);
        }



        showHighestScore();
    }

    private void showHighestScore() {

        tvCurrentScore.setText("You Score : " + currentScore);

        sp = getSharedPreferences("game_score", MODE_PRIVATE);
        int highestScore = sp.getInt("highest_score", -1);
        if (highestScore == -1) {
            if (currentScore == -1) {
                tvHighestScore.setText("You don't have any score!");
            }else{
                tvHighestScore.setText("Highest Score : "+currentScore);
                sp.edit().putInt("highest_score", currentScore).apply();
            }
        }else{
            if (highestScore < currentScore) {
                tvHighestScore.setText("You passed your previous record!");
                sp.edit().putInt("highest_score", currentScore).apply();
            }else{
                tvHighestScore.setText("Highest Score : "+highestScore);
            }
        }
    }
}
