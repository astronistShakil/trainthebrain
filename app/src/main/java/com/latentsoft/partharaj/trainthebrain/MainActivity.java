package com.latentsoft.partharaj.trainthebrain;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Puzzler puzzler;
    String colorString;
    ArrayList<String> colorCodes = new ArrayList<>();
    PuzzleAdapter adapter;

    TextView textView;
    TextView tvCorrect, tvWrong;

    RecyclerView recyclerView;

    int numCorrect = 0, numWorng = 0;

    boolean isGameRunning = true;

    Rocket rocket;

    private int levelInterval = 8000;
    private int interval = 6000;

    private int puzzleCount = 0;
    private RatingBar rbLife;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.textView);
        tvCorrect = (TextView) findViewById(R.id.tvCorrect);
        tvWrong = (TextView) findViewById(R.id.tvWrong);
        rbLife = (RatingBar) findViewById(R.id.ratingBar);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        showNextLevel();

        rbLife.setRating(3f);

        rocket = new Rocket();
        rocket.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isGameRunning = true;
    }

    private void showNextLevel() {

        rbLife.setRating((float) (2 - (Math.ceil(numWorng/3)-1)));

        if (numWorng == 9) {
            startActivity(new Intent(this, ResultActivity.class).putExtra("score",numCorrect));
            rocket.cancel(true);
            finish();
        }

        puzzleCount++;

        if (puzzleCount >= 10) {
            levelInterval -= 200;
            puzzleCount = 0;
        }

        puzzler = new Puzzler();
        recyclerView.setAdapter(null);

        puzzler.generate();

        colorString = puzzler.getColorString();
        colorCodes = puzzler.getColorCodes();

        textView.setText(colorString);

        adapter = new PuzzleAdapter(this, puzzler, colorCodes);
        adapter.setOnAnswerGivenListener(new PuzzleAdapter.OnAnswerGiven() {
            @Override
            public void onCorrect() {
                numCorrect++;
                tvCorrect.setText(numCorrect + "");
                showNextLevel();
                interval = levelInterval;
            }

            @Override
            public void onWrong() {
                numWorng++;
                tvWrong.setText(numWorng + "");
                showNextLevel();
                interval = levelInterval;
            }
        });

        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        isGameRunning = false;
    }

    class Rocket extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            numWorng++;
            tvWrong.setText(numWorng + "");
            showNextLevel();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            while(!isCancelled())
            while (isGameRunning) {
                try {
                    while (interval > 0) {
                        Thread.sleep(10);
                        interval -= 100;
                    }
                    interval = levelInterval;
                    publishProgress();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }
}

